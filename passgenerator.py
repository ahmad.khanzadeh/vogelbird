# easy way....
import random 
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))

generated_password=''

for letter in range(1,nr_letters+1):
    the_length=len(letters)
    the_number=random.randint(0,the_length-1)
    generated_password+=letters[the_number]


for the_symbol in range(1,nr_symbols+1):
    the_length=len(symbols)
    the_number=random.randint(0,the_length-1)
    generated_password+=symbols[the_number]

for the_number in range(1,nr_numbers+1):
    the_length=len(numbers)
    the_number=random.randint(0,the_length-1)
    generated_password+=numbers[the_number]
    
print(generated_password)